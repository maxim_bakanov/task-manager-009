package ru.mbakanov.tm.api;

import ru.mbakanov.tm.model.TerminalCommand;

import java.util.Arrays;

public interface ICommandRepository {

    String[] getCommands (TerminalCommand... values);

    String[] getArgs (TerminalCommand... values);

    String[] getCommands();

    String[] getArgs();

    TerminalCommand[] getTerminalCommands();

}
